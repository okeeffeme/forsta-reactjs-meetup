import forstaLogo from './assets/forsta.svg'
import reactLogo from './assets/react.svg'
import './App.css'

function App() {
  return (
    <div className="App">
      <div>
        <a href="https://forsta.com" target="_blank">
          <img src={forstaLogo} className="logo" alt="Forsta logo" />
        </a>
          <a href="https://www.meetup.com/reactjs-oslo-meetup/" target="_blank">
              <img src={reactLogo} className="logo" alt="ReactJS Meetup" />
          </a>
      </div>
      <h1>Welcome ReactJS Meetup Members</h1>

        <h2>Need something while you're here?</h2>
        <ul>
            <li>Duda Fernandas <a href='tel:+47 45 72 15 21'>+47 45 72 15 21</a></li>
            <li>Jacek Tessen-Wesierski <a href='tel:+47 45 83 55 62'>+47 45 83 55 62</a></li>
        </ul>

      <section>
          <h2>Agenda:</h2>
            <ul className='schedule'>
                <li><strong>17.30</strong> Pizza and drinks</li>
                <li><strong>18.00</strong> Welcome and introduction</li>
                <li><strong>18.10</strong> Design system in React (Megan O'Keeffe)</li>
                <li><strong>18.40</strong> Break</li>
                <li><strong>18.55</strong> Module Federation (Einar Paul Qvale)</li>
                <li><strong>19.25 to 21.00</strong> Mingling!</li>
            </ul>
      </section>
        <section>
            <h2>Important links:</h2>
            <ul>
                <li><a href="https://www.meetup.com/reactjs-oslo-meetup/events/288282813/" target="_blank">Link to the Meetup.com event</a></li>
                <li><a href="https://app.sli.do/event/3RWnshgjaYEuq4tv8UvApG/live/questions" target="_blank">Ask questions for the Q&A</a></li>
                <li><a href="https://www.forsta.com/company/careers/" target="_blank">See our open positions</a></li>
            </ul>
        </section>
        <section>
            <h2>Slides:</h2>
            <p>We'll post the slides here after the event!</p>
        </section>
      <p className="thanks">
        Thanks for coming!
      </p>
    </div>
  )
}

export default App
